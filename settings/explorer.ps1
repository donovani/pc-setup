########################
# File Explorer Settings
########################

# Create personal folders in Home Directory
cd $HOME
mkdir ".ssh"
mkdir "Markdown"
mkdir "Repos"
cd Repos
mkdir "Personal"
mkdir "Game Dev"
mkdir "ScheduleLab"

# Create a personal SSH keypair (no passphrase) in ~/.ssh
ssh-keygen -b 2048 -t rsa -f $HOME\.ssh\id_rsa -q -N '""'

# Remove OneDrive Folder
cd $HOME
rm -r -Force "OneDrive"

# Disable Quick Access: Recent Files
Set-ItemProperty -Path HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer -Name ShowRecent -Type DWord -Value 0

# Disable Quick Access: Frequent Folders
Set-ItemProperty -Path HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer -Name ShowFrequent -Type DWord -Value 0

# Unpin from Quick Access - Documents/Desktop/Downloads/Pictures
$QuickAccess = New-Object -ComObject shell.application
$QuickAccessPins = $QuickAccess.Namespace("shell:::{679f85cb-0220-4080-b29b-5540cc05aab6}").Items()
($QuickAccessPins | where {$_.Path -eq "$HOME\Desktop"}).InvokeVerb("unpinfromhome")
($QuickAccessPins | where {$_.Path -eq "$HOME\Documents"}).InvokeVerb("unpinfromhome")
($QuickAccessPins | where {$_.Path -eq "$HOME\Downloads"}).InvokeVerb("unpinfromhome")
($QuickAccessPins | where {$_.Path -eq "$HOME\Pictures"}).InvokeVerb("unpinfromhome")

# Pin desired folders to Quick Access
$QuickAccess.Namespace($HOME).Self.InvokeVerb("pintohome")
$QuickAccess.Namespace("$HOME\AppData").Self.InvokeVerb("pintohome")
$QuickAccess.Namespace("$HOME\Repos").Self.InvokeVerb("pintohome")
$QuickAccess.Namespace("$HOME\Repos\Game Dev").Self.InvokeVerb("pintohome")
$QuickAccess.Namespace("$HOME\Markdown").Self.InvokeVerb("pintohome")

# Hide useless folders
gi "$Home\3D Objects","$Home\Contacts","$Home\Favorites","$Home\Links","$Home\Saved Games","$Home\Searches" -Force | foreach { $_.Attributes = $_.Attributes -bor "Hidden" }
