# This file tracks settings changes that I like to have but are difficult/impossible to do in Powershell
# It's easier to do them manually for now, and just print out a reminder on how to do so.

echo "The following settings have not been set, and must be changed manually:"

echo "Accounts > Your Info > Create your picture -> BROWSE"
echo "Apps > Default Apps > Web Browser -> BRAVE/FIREFOX"
echo "Ease of Access > Mouse Pointer > Change pointer color -> BLACK"
echo "Search > Cloud Content Search > Microsoft account -> OFF"
echo "Search > Cloud Content Search > Work or school account -> OFF"
echo "System > About > Device Specifications > Rename this PC -> COMPY/LAPPY"
echo "System > Multitasking > Timeline > Show suggestions in your timeline -> OFF"
echo "Personalization > Background -> BROWSE"
echo "Personalization > Colors > Transparcy effects -> OFF"
echo "Personalization > Colors > Choose your accent color -> SELECT"
echo "Personalization > Colors > Show accent color on: Start, taskbar, & action center -> ON"
echo "Personalization > Lock Screen > Background -> SELECT"
echo "Personalization > Start > Show suggeestions occasionally in Start -> OFF"

