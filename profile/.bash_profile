#######################
# Environment Variables
#######################

export REPO_PATH = $HOME\Repos

#########
# Aliases
#########



###################
# Utility Functions
###################

// Takes a repo name as $1 and cd's to it from REPO_PATH
repo () {
  cd $REPO_PATH
  cd $(find ./ -type d -iname $1 2> NUL)
}
