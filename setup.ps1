# Navigate to home directory to avoid issues with permissions
cd %HOMEPATH%

# First essential step installs Chocolatey
# Commands are taken directly from their page
Set-ExecutionPolicy Bypass -Scope Process -Force
[System.Net.ServicePointManager]::SecurityProtocol = [System.Net.ServicePointManager]::SecurityProtocol -bor 3072
iex ((New-Object System.Net.WebClient).DownloadString('https://community.chocolatey.org/install.ps1'))

# Next we enable global confirmation in Chocolatey
# This makes it so we don't have to confirm every install
choco feature enable -n allowGlobalConfirmation
