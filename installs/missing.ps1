# This file tracks installs that I like to have but are unavailable to be automated via Chocolatey.
# It simply lists them off and provides a link to download the latest version from manually.

echo "The following programs must be installed manually:"

echo "The following tools aren't available on Chocolatey:"
echo "Sophos Home:   https://home.sophos.com/en-us"
echo "Wacom Drivers: https://www.wacom.com/en-us/support/product-support/drivers"
echo "           or: https://cdn.wacom.com/u/productsupport/drivers/win/professional/WacomTablet_6.3.45-1.exe"

echo "The following tools aren't up-to-date on Chocolatey:"
echo "ChromeDriver:  https://chromedriver.chromium.org/downloads"

echo "The following extensions must be manually added to browsers:"
echo "Bitwarden:     https://bitwarden.com/"
echo "DarkReader:    https://darkreader.org/"
echo "uBlock Origin: https://ublockorigin.com/"
