# Personal content-browsing
choco install brave
choco install firefox
choco install discord

# Common utilities
choco install bitwarden
choco install obsidian
choco install sharex

# Common file-opening tools
choco install 7zip
choco install imageglass
choco install foxitreader
choco install vlc

# Essential driver tools
choco install geforce-experience
