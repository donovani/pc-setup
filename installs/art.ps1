# Raster art/painting
choco install krita

# Animation
choco install opentoonz
choco install ffmpeg

# 3D Modeling
choco install blender
