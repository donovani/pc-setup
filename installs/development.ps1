# First things first, install Git
choco install git
choco install git-fork --ignore-checksums

# Code runtime environments
choco install jdk11
choco install python

# Install development tools
choco install hyper
choco install vscode
choco install visualstudio2022community
choco install visualstudio2022-workload-managedgame
choco install intellijidea-community

# Containerization
choco install docker-desktop

# Webdev installations
choco install nvm
choco install postman
choco install mysql
choco install postgresql

# Gamedev installations
choco install unity-hub

# Mobile development installs
choco install androidstudio
