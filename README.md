# PC Setup

I've had to reset my computers several times this year and it's gotten to the point where I'm considering automating all my installs so I can just setup everything with a curl command. This is a personal project, but feel free to fork/copy it.

## Running the Setup Script

From an Admin-level Powershell window, run the following script:

```sh
curl 'idk what goes here yet'
```

## What the Script Does

The script operates by installing the latest version of Chocolatey package manager, then installing all desired dependencies. This repo also includes a personal command line profile for common things I do. Naturally this means the script can modify the installs on the device, as well as the PATH, so use it with care, and test changes in an environment where it's safe to break things.

* `--all` - Installs all program categories listed below
* `--core` - Installs the essential programs I use all the time (browser, Discord, etc.)
* `--art` - Installs any art programs (Krita, Blender, etc.)
* `--audio` - Installs audio editing tools and DAWs (Audacity, LMMS, etc.)
* `--business` - Installs tools I use specifically for business (Slack, Figma, etc.)
* `--development` - Installs tools for various development purposes (VS, Unity, etc.)
* `--games` - Installs game services, optionally to another drive (Steam, EGS, etc.)
* `--video` - Installs video recording/editing tools (OBS, Premiere, etc.)
